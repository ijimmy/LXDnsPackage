##介绍：
一个c#开发的dns协议解析包。可以生成查询请求，接收返回的数据，并解析出结果。

##应用场景：

如果只是想根据域名获取ip地址，可以使用.net自带的类“System.Net.Dns.GetHostByName(string UriHostName)” 简单获取。

但如果需要获得其他域名相关信息，如MX记录、SOA记录、TXT记录、CNAME记录等，显然就不那么容易了。

一般的做法是调用系统的api：“dnsapi"，或者使用nslookup获取信息然后重定向他的输出记录并分析。

以上两类方法各有缺点，如使用api，比较麻烦；使用nslookup可能速度上不太如意，还要判断很多信息。

鉴于以上问题，可以使用MyDnsPackage包来获取相应信息。

##使用方法：
具体例子请看项目中的`MyDnsForm`示例项目。

    MyDns mydns = new MyDns();
    if (!mydns.Search("www.lixin.me", QueryType.A, "8.8.8.8", null ))
    {
    
    MessageBox.Show(mydns.header.RCODE.ToString());
    return;
    }
    txtInfo.Clear();
    txtInfo.AppendText (string.Format ("回复记录数：{0}\n",mydns.header.ANCOUNT) );
    txtInfo.AppendText(string.Format("回复额外记录数：{0}\n", mydns.header.ARCOUNT ));
    txtInfo.AppendText(string.Format("回复权威记录数：{0}", mydns.header.NSCOUNT ));
    txtContent.Clear();
    foreach (MyDnsRecord item in mydns.record.Records)
    {
      txtContent.AppendText(item.QType.ToString() + "   " + item.RDDate.ToString()+"\n");
    }

![](http://git.oschina.net/lixin/LXDnsPackage/raw/master/pic1.jpg)